import Foundation

protocol GamePresenter: class {
    var view: GameView? { get set }
    
    func gameStarted()
    func guessResult(_ result: GuessResult)
}

class GamePresenterImpl: GamePresenter {
    weak var view: GameView?
    
    func gameStarted() {
//        view?.newGameStarted()
        view?.update(state: .newGame)
    }
    
    func guessResult(_ result: GuessResult) {
        switch result {
        case .outOfRangeLow:
            view?.update(state: .guess(response: "Thats too low. Pick a number greater than or equal to 1"))
        case .outOfRangeHigh:
            view?.update(state: .guess(response: "Thats too high. Pick a number less than or equal to 100"))
        case .tooLow:
            view?.update(state: .guess(response: "Too low, guess higher"))
        case .tooHigh:
            view?.update(state: .guess(response: "Too high, guess lower"))
        case .correct:
            view?.update(state: .guess(response: "CORRECT!"))
        }
    }
}
