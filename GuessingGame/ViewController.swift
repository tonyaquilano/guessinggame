import UIKit

enum GuessState {
    case newGame
    case guess(response: String)
}

protocol GameView: class {
    func newGameStarted()
    func updateResponse(text: String)
    func update(state: GuessState)
}

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var responseLabel: UILabel!
    
    let presenter: GamePresenter = GamePresenterImpl()
    let interactor: GameInteractor = GameInteractorImpl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        textField.becomeFirstResponder()
        responseLabel.numberOfLines = -1
        
        interactor.presenter = presenter
        presenter.view = self
        
        interactor.startNewGame()
    }

    @IBAction func tappedGuessButton(_ sender: Any) {
        if let number = Int(textField.text!) {
            interactor.guess(number: number)
        }
    }
    @IBAction func tappedNewGameButton(_ sender: Any) {
        interactor.startNewGame()
    }
    
}

extension ViewController: GameView {
    func update(state: GuessState) {
        switch state {
        case .newGame:
            newGameStarted()
        case .guess(let response):
            updateResponse(text: response)
        }
    }
    
    func newGameStarted() {
        textField.text = ""
        responseLabel.text = ""
    }
    
    func updateResponse(text: String) {
        responseLabel.text = text
    }
}

