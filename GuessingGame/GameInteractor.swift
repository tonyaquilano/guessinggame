import Foundation

enum GuessResult {
    case outOfRangeLow
    case outOfRangeHigh
    case tooLow
    case tooHigh
    case correct
}

protocol GameInteractor: class {
    var presenter: GamePresenter? { get set }
    
    func startNewGame()
    func guess(number: Int)
}

class GameInteractorImpl: GameInteractor {
    
    var presenter: GamePresenter?
    
    var randomNumber = 0
    
    func startNewGame() {
        randomNumber = Int.random(in: 1 ... 100)
        
        presenter?.gameStarted()
    }
    
    func guess(number: Int) {
        
        if number <= 0 {
            presenter?.guessResult(.outOfRangeLow)
            return
        }
        
        if number > 100 {
            presenter?.guessResult(.outOfRangeHigh)
            return
        }
        
        if number < randomNumber {
            presenter?.guessResult(.tooLow)
            return
        }
        
        if number > randomNumber {
            presenter?.guessResult(.tooHigh)
            return
        }
        
        if number == randomNumber {
            presenter?.guessResult(.correct)
            return
        }
    }
}
